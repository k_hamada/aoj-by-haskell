{-
Plastic Board

機械に辺・対角線の長さのデータを入力し、プラスティック板の型抜きをしている工場があります。
この工場では、サイズはいろいろですが、平行四辺形の型のみを切り出しています。
あなたは、切り出される平行四辺形のうち、長方形とひし形の製造個数を数えるように上司から命じられました。
「機械に入力するデータ」を読み込んで、長方形とひし形の製造個数を出力して終了するプログラムを作成してください。

Input
「機械に入力するデータ」は、平行四辺形の隣り合う２辺の長さと対角線の長さが記録されています。
おのおのの長さは全て整数で与えられます。入力例を参考にしてください。

辺1 の長さ, 辺2 の長さ, 対角線の長さ（1 件目のデータ：全て整数）
              ... 　　　　　　　　　（2 件目のデータ：全て整数）
              ... 　　　　　　　　　（3 件目のデータ：全て整数）
              ... 　　　　　　　　　　　　　　　

Output
長方形の製造個数
ひし形の製造個数

Sample Input
3,4,5
5,5,8
4,4,4
5,4,3

Output for the Sample Input
1
2
-}

import Data.List.Split (splitOn)

parse :: String -> [Int]
parse = map read . splitOn ","

rectangle :: [Int] -> Bool
rectangle [x, y, z] = x^2 + y^2 == z^2

rhombus :: [Int] -> Bool
rhombus [x, y, _] = x == y

main = do
    contents <- fmap (map parse . words) $ readFile "0032.txt"
    mapM_ (print . length . flip filter contents) [rectangle, rhombus]

