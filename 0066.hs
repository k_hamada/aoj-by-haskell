{-
Tic Tac Toe

三目並べは，３×３のマス目の中に交互に○と×を入れていき、
縦・横・斜めの何れかに一列○か×が並んだときに、そちらの勝ちとなるゲームです（図1〜図3 を参照）

図１：○の勝ち	図２：×の勝ち	図３：引き分け

三目並べは、○と×が交互にマス目を埋めていき、どちらかが一列揃ったときにゲーム終了となります。
そのため、図 4 のように、○と×が両方とも一列そろっている場合はありえない局面です。ありえない局面が入力されることはありません。

図４：ありえない局面

三目並べの盤面を読み込んで、それぞれについて勝敗の結果を出力して終了するプログラムを作成して下さい。

Input
盤面の入力は、○、×、空白をそれぞれ半角英小文字の o、x、s であらわし、1 件につき 1 行に、下の図マス目の順に並んでいます。
入力例を見てください。

Output
○が勝ちなら半角英小文字の o を、×が勝ちなら半角英小文字の x を、引き分けならば半角英小文字の d を出力してください。

勝敗（1 件目のデータに対する出力）
勝敗（2 件目のデータに対する出力）
...
...

Sample Input
ooosxssxs
xoosxsosx
ooxxxooxo

Output for the Sample Input
o
x
d
-}

import Data.List (transpose)
import Data.List.Split (chunksOf)

isWin :: Char -> String -> Bool
isWin a xs =
    or [
      check . isA a $ xs,
      check . transpose . isA a $ xs,
      checkX . isA a $ xs,
      checkX . reverse . isA a $ xs
    ]
  where
    isA a xs = chunksOf 3 . map (== a) $ xs
    check = elem [True, True, True]
    checkX [[True, _, _], [_, True, _], [_, _, True]] = True
    checkX _ = False

solve :: String -> String
solve xs
    | isWin 'o' xs = "o"
    | isWin 'x' xs = "x"
    | otherwise    = "d"

main = do
    contents <- fmap lines $ readFile "0066.txt"
    mapM_ (putStrLn . solve) contents

