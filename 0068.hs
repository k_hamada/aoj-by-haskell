{-
Enclose Pins with a Rubber Band

n 本の釘を平板上の座標 P1(x1, y1), P2(x2, y2), P3(x3, y3),,,, Pn(xn, yn) に１本ずつ打ち、
輪ゴムの輪の中に全ての釘が入るように 1 本の輪ゴムで囲みます。
このとき、輪ゴムが交差してはいけません。
釘の座標を読み込んで、上記のように釘を輪ゴムで囲んだときに輪ゴムに接していない釘の本数を出力して終了するプログラムを作成してください。
輪ゴムは充分に伸び縮みするものとします。
同じ座標に 2 本以上の釘を打つことはないものとします。
また、輪ゴムがかかった釘と釘の間は直線で結ばれるものとし、その直線上に 3 本以上の釘が並ぶことはないものとします。
例えば、図 1 に示すような入力はありえません。
図 2 に示すように輪ゴムがかかっていない釘が 1 直線上に並ぶことはありえます。

図１	図２

ただし、それぞれの座標値は -1000.0 以上1000.0 以下の実数です。また、n は 3 以上100 以下の整数です。

Input
複数のデータセットが与えられます。各データセットは以下のような形式ですｌ。n が 0 の時、入力の最後とします。

n
x1, y1（全て実数）
x2, y2（全て実数）
...
...
xn, yn（全て実数）

Output
ゴムと接していない釘の本数を出力してください。
例えば、図 3 に示す４つの釘を表す入力があった場 合、図 4 のように囲まれるので、輪ゴムに接していない釘の本数は 1 本です。

図３	図４

Sample Input
4
1.0,0.0
0.0,1.0
2.0,1.0
1.0,2.0
9
-509.94,892.63
567.62,639.99
-859.32,-64.84
-445.99,383.69
667.54,430.49
551.12,828.21
-940.2,-877.2
-361.62,-970
-125.42,-178.48
0

Output for the Sample Input
0
3

Hint
以下は２つめのサンプル入力に対する図です。
-}

import Data.List.Split (splitOn)


-- http://www.haskell.org/haskellwiki/Graham_Scan_Implementation
-- http://www.ccad.sist.chukyo-u.ac.jp/~mito/ss/program/C/DialogBase/algo/Graham/
-- http://d.hatena.ne.jp/yamamucho/20100322/1269245323

import Data.Ord (comparing)
import Data.List (sortBy)

-- 点同士の位置関係型を定義
data Direction = LeftTurn
               | RightTurn
               | Straight
                 deriving (Show, Eq)

-- 点の座標型を定義
data Point = Point (Double, Double)
             deriving (Show)

-- 三点の位置関係を計算し、時計回り（凸）かどうかの判断
dir :: Point -> Point -> Point -> Direction
dir (Point (ax, ay)) (Point (bx, by)) (Point (cx, cy)) =
    case sign of
         EQ -> Straight
         GT -> LeftTurn
         LT -> RightTurn
  where
    sign = compare ((bx - ax) * (cy - ay)) ((by - ay) * (cx - ax))

-- dirを点のリストに適用
dirlist :: [Point] -> [Direction]
dirlist (x:y:z:xs) = dir x y z : dirlist (y:z:xs)
dirlist _ = []

-- （起点を求めるため）Y軸で整列
sortByY :: [Point] -> [Point]
sortByY =
    sortBy lowestY
  where
    lowestY (Point (x1, y1)) (Point (x2, y2)) = compare (y1, x1) (y2, x2)

-- 点同士の角度を比較
compareAngles :: Point -> Point -> Point -> Ordering
compareAngles =
    comparing . pointAngle
  where
    pointAngle (Point (x1, y1)) (Point (x2, y2)) = (x2 - x1) / (y2 - y1)

-- 基点との角度順に整列
-- >   起点 : [起点との角度で整列した点]
sortByAngle :: [Point] -> [Point]
sortByAngle ps =
    bottomLeft : sortBy (compareAngles bottomLeft) (tail sortedPs)
  where
    sortedPs   = sortByY ps
    bottomLeft = head sortedPs

-- Graham Scan
gscan :: [Point] -> [Point]
gscan =
    scan . sortByAngle
  where
    scan (x:y:z:xs) = if dir x y z == LeftTurn  -- もし先頭三つの形が時計回り（凸）でないなら
                         then scan (x:z:xs)     -- 候補を一つ除外して継続
                         else x : scan (y:z:xs) -- 一番目の候補を確定し、残りを継続
    scan [x,y]      = [x, y]
    scan _          = []


parse :: [String] -> [[Point]]
parse [] = []
parse (x:xs) =
    map toPoint ys : parse zs
  where
    n        = read x
    (ys, zs) = splitAt n xs
    toPoint  = (\[x, y] -> Point (x, y)) . map read . splitOn ","

solve :: [Point] -> Int
solve xs =
    (length xs -) . length . gscan $ xs

main = do
    contents <- fmap (parse . fst . break (== "0") . lines) $ readFile "0068.txt"
    mapM_ (print . solve) contents

