{-
Hit and Blow

ヒットアンドブローというゲームがあります。
Aさんが異なる４個の数字を思い浮かべ、Bさんがその数字を当てます。
Bさんが選んだ４個の数字に対し、Aさんは、
  場所が一致している数字の個数（ヒット）
  場所は違うが正解の中に含まれる数字の個数（ブロー）
を答えます。
それを手がかりに、Bさんは再び４個の数字を選んでAさんに伝えます。

例えば、Aさんが
  9 1 8 2
という数字を思い浮かべたとします。このときBさんが
  4 1 5 9
という数字を選んだら、Aさんは「１ヒット１ブロー」と答えます。

Aさんの思い浮かべた４個の数字と、Bさんが選んだ４個の数字を入力してヒットとブロー数の数を出力して終了するプログラムを作成して下さい。
ただし、４個の数字は０から９の範囲で、すべて異なります。

Input
複数のデータセットが与えられます。各データセットは以下のような形式です。
  a1 a2 a3 a4
  b1 b2 b3 b4

ここで、
  a1 (Aさん１個目の数：０から９の整数)
  a2 (Aさん２個目の数：０から９の整数)
  a3 (Aさん３個目の数：０から９の整数)
  a4 (Aさん４個目の数：０から９の整数)
  b1 (Bさん１個目の数：０から９の整数)
  b2 (Bさん２個目の数：０から９の整数)
  b3 (Bさん３個目の数：０から９の整数)
  b4 (Bさん４個目の数：０から９の整数)
とします。入力の最後まで処理して下さい。

Output
各データセットに対して、ヒット数とブロー数を１行に出力して下さい。なお、ヒット数とブロー数は１つのスペースで区切って下さい。

Sample Input
9 1 8 2
4 1 5 9
4 6 8 2
4 6 3 2

Output for the Sample Input
1 1
3 0
-}

import Data.List (intersect)
import Data.List.Split (chunksOf)

solve :: [[Int]] -> String
solve [xs, ys] =
  show h ++ " " ++ show b
  where
    h = hit xs ys
    b = length (xs `intersect` ys) - h

hit :: [Int] -> [Int] -> Int
hit xs ys =
  sum . map fromEnum $ zipWith (==) xs ys
  -- http://stackoverflow.com/questions/573751/using-foldl-to-count-number-of-true-values


main = do
  contents <- fmap (chunksOf 2 . map (map read . words) . lines) $ readFile "0025.txt"
  mapM_ (putStrLn . solve) contents

