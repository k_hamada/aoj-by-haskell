{-
Parallelism

A = (x1, y1), B = (x2, y2), C = (x3, y3), D = (x4, y4) の異なる４つの座標点が与えられたとき、
直線 AB と CD が平行かどうかを判定し、
平行である場合には YES、平行でない場合には NO と出力して終了するプログラムを作成して下さい。

なお、x1, y1, x2, y2, x3, y3, x4, y4 は、それぞれ -100 以上 100 以下とします。

Input
複数のデータセットが与えられます。
一行目にデータセットの数 n が与えられます。
つづいて n 行のデータセットが与えられます。
各データセットの形式は以下のとおりです。

x1 y1 x2 y2 x3 y3 x4 y4
各値は実数です。

Output
各データセットに対して、YES または NO を１行に出力して下さい。

Sample Input
2
0.0 0.0 1.0 1.0 1.0 0.0 2.0 1.0
3.0 2.0 9.0 6.0 13.0 5.0 7.0 9.0

Output for the Sample Input
YES
NO
-}

import Data.List.Split (chunksOf)

-- http://d.hatena.ne.jp/z_u_m_i/20050605/1118040889
-- http://www.atelier-blue.com/contest/pc-concours/2003-h/2003h01-25.htm

type Point = (Double, Double)

minus' :: Point -> Point -> Point
minus' (x1, y1) (x2, y2) = (x2 - x1, y2 - y1)

check :: (Point, Point) -> Bool
check (p, q)
  | p == q = True
  | p' == q' = True
  | otherwise = False
  where
    f (x, y) = y / x
    p' = f p
    q' = f q

solve :: [Point] -> Bool
solve [a, b, c, d] =
  check (minus' a b, minus' c d)

yesno :: Bool -> String
yesno True = "YES"
yesno _    = "NO"


main = do
  contents <- fmap (drop 1 . lines) $ readFile "0021.txt"
  mapM_ (putStrLn . yesno . solve . map (\[x, y] -> (x, y)) . chunksOf 2 . map read . words) contents

