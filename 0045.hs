{-
Sum and Average
販売単価と販売数量を読み込んで、販売金額の総合計と販売数量の平均を出力して終了するプログラムを作成してください。

Input
販売単価, 販売数量（全て整数）
販売単価, 販売数量（全て整数）
販売単価, 販売数量（全て整数）
...
...

Output
販売金額の総合計（整数）
販売数量の平均(整数）
※販売数量の平均に端数（小数点以下の数）が生じた場合は小数点以下第1 位を四捨五入してください。

Sample Input
100,20
50,10
70,35

Output for the Sample Input
4950
22
-}

import Data.List.Split (splitOn)

parse :: String -> [Int]
parse =
    map read . splitOn ","

average :: [Int] -> Int
average xs =
    round $ fromIntegral (sum xs) / fromIntegral (length xs)

main = do
    contents <- fmap (map parse . lines) $ readFile "0045.txt"
    print . sum . map product $ contents
    print . average . map last $ contents

