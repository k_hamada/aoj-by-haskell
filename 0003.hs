{-
Is it a Right Triangle?

1000 以下の３つの正の整数を入力し、それぞれの長さを３辺とする三角形が直角三角形である場合には YES を、違う場合には NO と出力して終了するプログラムを作成して下さい。

Input
複数のデータセットが与えられます。１行目にデータセット数 N が与えられます。続いて N 行の入力が与えれます。各行に３つの整数が１つのスペースで区切られて与えられます。

Output
各データセットごとに、YES または NO を１行に出力して下さい。
-}

import Data.List (sort)

isRightTriangle :: [Int] -> Bool
isRightTriangle [x, y, z] = (x^2 + y^2) == z^2

yesno :: Bool -> String
yesno True = "YES"
yesno _    = "NO"

main = do
  contents <- fmap (drop 1 . lines) $ readFile "0003.txt"
  mapM_ (putStrLn . yesno . isRightTriangle . map read . words) contents

