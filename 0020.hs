{-
Capitalize

文字列に含まれる半角英小文字を半角英大文字に置き換えて、その文字列を出力して終了するプログラムを作成して下さい。
半角英小文字以外の文字は置き換えないで下さい。

Input
半角英小文字、ピリオド、空白のみを含む文字列

Output
半角英小文字を半角英大文字に置き換えた文字列

Sample Input
this is a pen.

Output for the Sample Input
THIS IS A PEN.
-}

import Data.Char (toUpper)

main = do
  contents <- fmap lines $ readFile "0020.txt"
  mapM_ (putStrLn . map toUpper) contents

