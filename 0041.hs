{-
Expression

与えられた 4 つの 1 から 9 の整数を使って、答えが 10 になる式をつくります。
4 つの整数 a, b, c, d を入力したとき、下記の条件に従い、答えが 10 になる式を出力して終了するプログラムを作成してください。
また、答が複数ある時は、最初に見つかった答だけを出力するものとします。
答がない時は、0 と出力してください。

演算子として、加算 (+)、減算 (-)、乗算 (*) だけを使います。除算 (/) は使いません。
数を4つとも使わなければいけません。
4つの数の順番は自由に入れ換えてかまいません。
カッコを使ってもかまいません。

Input
複数のデータセットが与えられます。各データセットの形式は以下のとおり：
  a b c d
入力は４つの0で終了します。

Output
各データセットについて、与えられた 4 つの整数と上記の演算記号およびカッコを組み合わせて値が 10 となる式または 0　を１行に出力してください。

Sample Input
8 7 9 9
4 4 4 4
5 5 7 5
0 0 0 0

Output for the Sample Input
((9 * (9 - 7)) - 8)
0
((7 * 5) - (5 * 5))
-}

import Data.List (permutations)

type RPN = String

toRPN :: [Int] -> [RPN]
toRPN xs =
    map unwords . concat $ [type1, type2, type3, type4, type5]
  where
    [a, b, c, d] = map show xs
    op = ["+", "-", "*"]
    type1 = [[a, b, x, c, y, d, z] | x <- op, y <- op, z <- op] -- a b # c # d #
    type2 = [[a, b, x, c, d, y, z] | x <- op, y <- op, z <- op] -- a b # c d # #
    type3 = [[a, b, x, y, c, d, z] | x <- op, y <- op, z <- op] -- a b c # # d #
    type4 = [[a, b, c, x, d, y, z] | x <- op, y <- op, z <- op] -- a b c # d # #
    type5 = [[a, b, c, d, z, y, z] | x <- op, y <- op, z <- op] -- a b c d # # #
  -- http://www18.ocn.ne.jp/~sign/program/contest/aoj0041.html

-- http://learnyouahaskell.com/functionally-solving-problems#reverse-polish-notation-calculator
solveRPN :: (Num a, Read a) => RPN -> a
solveRPN = head . foldl foldingFunction [] . words
    where   foldingFunction (x:y:ys) "*" = (x * y):ys
            foldingFunction (x:y:ys) "+" = (x + y):ys
            foldingFunction (x:y:ys) "-" = (y - x):ys
            foldingFunction xs numberString = read numberString:xs

transRPNtoInfix :: RPN -> String
transRPNtoInfix = head . foldl foldingFunction [] . words
    where   foldingFunction (x:y:ys) "*" = strOp "*" x y:ys
            foldingFunction (x:y:ys) "+" = wrap (strOp "+" x y):ys
            foldingFunction (x:y:ys) "-" = wrap (strOp "-" y x):ys
            foldingFunction xs numberString = numberString:xs
            strOp op x y = unwords [x, op, y]
            wrap str = "("++str++")"

solve :: [Int] -> Maybe RPN
solve =
    head' . filter ((==) 10 . solveRPN) . concatMap toRPN . permutations
  where
    head' []     = Nothing
    head' (x:xs) = Just x

main = do
    contents <- fmap (fst . break (== [0, 0, 0, 0]) . map (map read . words) . lines) $ readFile "0041.txt"
    mapM_ (maybe (putStrLn "0") (putStrLn . transRPNtoInfix) . solve) contents

