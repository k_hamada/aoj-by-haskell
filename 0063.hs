{-
Palindrome

半角アルファベット文字列からなる、1 行あたり 100 文字以内のデータがあります。
いくつかの行は対称（左端から読んでも右端から読んでも同じ）です。
このデータを読み込んで、その中の対称な文字列行数を出力して終了するプログラムを作成してください。
なお、１文字だけからなる行は対称であるとします。

Input
複数の行からなる文字列

Output
対称な文字列の行数

Sample Input
abcba
sx
abcddcba
rttrd

Output for the Sample Input
2
-}

isSymmetry :: String -> Bool
isSymmetry s =
    take n s == (take n . reverse $ s)
  where
    n = length s `div` 2

main = do
    contents <- fmap lines $ readFile "0063.txt"
    print . length . filter isSymmetry $ contents

