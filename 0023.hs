{-
Circles Intersection

中心が (xa, ya) で半径 ra の円 A と、中心が (xb, yb) で半径 rb の円 B が与えられたとき、
  B が A の中にあるとき 2、
  A が B の中にあるとき -2、
  A の円周と B の円周が交わっている場合 1、
  A と B が重なっていないとき 0
を出力して終了するプログラムを作成して下さい。
なお A と B は同じものではないとします。

Input
複数のデータセットが与えられます。
１行目にデータセット数 N が与えられます。
つづいて N 行の入力が与えられます。
各データセットの形式は以下のとおりです。

  xa ya ra xb yb rb

ここで、入力の値はすべて実数です。

Output
各データセットに対して、2, -2, 1, 0 のいずれかの数値を１行に出力して下さい。

Sample Input
2
0.0 0.0 5.0 0.0 0.0 4.0
0.0 0.0 2.0 4.1 0.0 2.0

Output for the Sample Input
2
0
-}

type Point = (Double, Double)
type Radius = Double

distance :: Point -> Point -> Double
distance (xa, ya) (xb, yb) =
  sqrt $ (xb - xa) ^ 2 + (yb - ya) ^ 2

-- http://d.hatena.ne.jp/s_shige/20110822/p1
solve :: Radius -> Radius -> Double -> Int
solve ra rb d
  | d > ra + rb = 0
  | d < ra - rb = 2
  | d < rb - ra = -2
  | otherwise   = 1

main = do
  contents <- fmap (drop 1 . lines) $ readFile "0023.txt"
  mapM_ (print . (\[ax, ay, ar, bx, by, br] -> solve ar br (distance (ax, ay) (bx, by))) . map read . words) contents

