{-
Drawing Lots

下図のような阿弥陀くじがあります。
この例では縦線の数が５本あり、４本の横線が引かれています。横線は縦線をまたいでもよいとします。

図の上部では 1, 2, 3, 4, 5 の順番であったものが、２番目と４番目の間での交換（これを (2, 4) のように書く）をし、
さらに、(3, 5)(1, 2)(3, 4) の交換をしたので、図の下部に来たときは、4, 1, 2, 5, 3 になりました。

最初の状態　1, 2, 3, 4.... が、与えられた横棒のリスト（この例では、(2, 4)(3, 5)(1, 2)(3, 4) を通過したのち、
どのような順番になるかを出力して終了するプログラムを作成して下さい。

Input
縦線の本数 w (整数：w ≦ 30)
横棒の本数 n (整数：n ≦ 30)
横棒リスト a1, b1 (整数：半角カンマ区切り)
横棒リスト a2, b2 (整数：半角カンマ区切り)
.
.
.
横棒リスト an, bn (整数：半角カンマ区切り)

Output
左から右に、
1 番目の棒に来る数字
2 番目の棒に来る数字
.
.
w 番目の棒に来る数字

Sample Input
5
4
2,4
3,5
1,2
3,4

Output for the Sample Input
4
1
2
5
3
-}

import Data.List.Split (splitOn)

-- http://jutememo.blogspot.jp/2011/04/haskell-swap-9.html
swap (i:j:_) xs =
  swap' withIdx
  where
    withIdx = zip [0..] xs
    swap' = map f
      where
        i' = i - 1
        j' = j - 1
        f (idx, x) | idx == i'  = xs !! j'
                   | idx == j'  = xs !! i'
                   | otherwise = x

main = do
  contents <- fmap lines $ readFile "0011.txt"
  let w:h:_ = take 2 contents
      abs = map (map read . splitOn ",") . drop 2 $ contents
  mapM_ print $ foldl1 (.) (reverse . map swap $ abs) [1..read w]

