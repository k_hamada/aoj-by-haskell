{-
Is it Convex?

平面上の異なる4 点 A(xa, ya), B(xb, yb), C(xc, yc), D(xd, yd) の座標を読み込んで、それぞれについて、
それら4 点を頂点とした四角形 ABCD に凹みがなければ YES、
凹みがあればNO と半角大文字で出力して終了するプログラムを作成してください。

なお、xa, ya, xb, yb, xc, yc, xd, yd は、それぞれ-100 以上100 以下の実数とします。

凹みのある四角形とは図1 のような四角形です。

なお、1 直線上に3 つ以上点が並ぶことはないものとします。
また、入力順に点を結んでいけば、四角形になる順番に点の座標が入力されるものとします。
（つまり、図2 のような形になる順番はありません。)

Input
xa,ya,xb,yb,xc,yc,xd,yd（1 件目のデータ:全て実数）
xa,ya,xb,yb,xc,yc,xd,yd（2 件目のデータ:全て実数）
xa,ya,xb,yb,xc,yc,xd,yd（3 件目のデータ:全て実数）
...
...

Output
YES または NO (1 件目のデータに対する出力)
YES または NO (2 件目のデータに対する出力)
YES または NO (3 件目のデータに対する出力)
...
...

Sample Input
0.0,0.0,1.0,0.0,1.0,1.0,0.0,1.0
0.0,0.0,3.0,0.0,1.0,1.0,1.0,3.0

Output for the Sample Input
YES
NO
-}

import Data.List.Split (splitOn, chunksOf)

data Point = Point Float Float deriving (Show)

parse :: String -> [Point]
parse =
    map chunkToPoint . chunksOf 2 . map read . splitOn ","
  where
    chunkToPoint [x, y] = Point x y

-- http://d.hatena.ne.jp/say_hello_to_okaoka/20120202/1328144399
-- http://www5d.biglobe.ne.jp/~tomoya03/shtml/algorithm/Intersection.htm
solve :: [Point] -> Bool
solve [a, b, c, d] =
    check a c b d && check b d a c
  where
    check (Point x1 y1) (Point x2 y2) (Point x3 y3) (Point x4 y4) =
      ((x1 - x2) * (y3 - y1) + (y1 - y2) * (x1 - x3)) *
      ((x1 - x2) * (y4 - y1) + (y1 - y2) * (x1 - x4)) < 0

yesno :: Bool -> String
yesno True = "YES"
yesno _    = "NO"

main = do
    contents <- fmap (map parse . words) $ readFile "0035.txt"
    mapM_ (putStrLn . yesno . solve) contents

