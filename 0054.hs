{-
Sum of Nth decimal places

a, b, n は、いずれも正の整数であるとします。
分数 a / b の小数第 i 位の数を f(i) とします(0 ≦ f(i) ≦ 9)。
このとき、i = 1 から n までの f(i) の和を s とします。
  s = f(1) + f(2) + ... + f(n)
a, b, n を読み込んで、それぞれについて s を出力して終了するプログラムを作成してください。

Input
a b n（1 件目のデータ：全て整数）
a b n（2 件目のデータ：全て整数）
a b n（3 件目のデータ：全て整数）
...
...

Output
s（1 件目のデータに対する出力：整数）
s（2 件目のデータに対する出力：整数）
s（3 件目のデータに対する出力：整数）
...
...

Sample Input
1 2 3
2 3 4
5 4 3
4 3 2

Output for the Sample Input
5
24
7
6
-}

import Data.Char (digitToInt)
import Data.Ratio

solve :: [Integer] -> Int
solve [a, b, c] =
    sumEach . toDecimalN . onlyDecimal . fromRational $ rational
  where
    rational = a % b
    onlyDecimal n = n - realToFrac (truncate n)
    toDecimalN = truncate . (10^c *)
    sumEach = sum . map digitToInt . show

main = do
    contents <- fmap (map (map read . words) . lines) $ readFile "0054.txt"
    mapM_ (print . solve) contents

