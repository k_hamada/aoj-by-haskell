{-
Factorial

整数 n を入力し、n の階乗を出力して終了するプログラムを作成して下さい。ただし、n は、1 以上 20 以下とします。

Input
n (整数)

Output
n の階乗

Sample Input
5

Output for the Sample Input
120
-}

factorial 0 = 1
factorial n = n * factorial (n-1)

main = do
  contents <- fmap (read . head . lines) $ readFile "0019.txt"
  print . factorial $ contents

