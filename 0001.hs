{-
List of Top 3 Hills

山や丘の高さをメートル単位で 1 から 10,000 までの範囲の整数で表した 10 個のデータがあります。
その 10 個のデータを読み込んで、その中で、高い順から３つ出力して終了するプログラムを作成して下さい。

Input
  山の高さ１（整数）　
  山の高さ２（整数）　
       .
       .
  山の高さ１０（整数）　

Output
  最も高い山の高さ
  ２番目に高い山の高さ
  ３番目に高い山の高さ
-}

import Data.List (sort)

solve :: [Int] -> [Int]
solve =
  take 3 . reverse . sort 

main = do
  contents <- fmap (map read . lines) $ readFile "0001.txt"
  mapM_ print $ solve contents

