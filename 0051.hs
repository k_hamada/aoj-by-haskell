{-
Differential II

8 個の 0 から 9 までの数字を入力したとき、その 8 個の数字を並べ替えてできる、
最大の整数と最小の整数の差を出力して終了するプログラムを作成してください。
並び替えてできる数は 00135569 のように 0 から始まってもよいものとします。

Input
複数のデータセットが与えられます。
１行目にデータセット数 n が与えられます。
続いて n 行のデータが与えられます。
各データは 8 個の数字の並び（0 から9 の数字）です。

Output
各データセットに対して、入力された数字を並べ替えてできる最大の整数と最小の整数の差を１行に出力して下さい。

Sample Input
2
65539010
65539010

Output for the Sample Input
96417531
96417531
-}

import Data.List (sort)

solve :: String -> Int
solve xs =
    read (reverse xs') - read xs'
  where
    xs' = sort xs

main = do
    contents <- fmap (drop 1 . lines) $ readFile "0051.txt"
    mapM_ (print . solve) contents

