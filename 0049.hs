{-
Blood Group

ある学級の生徒の出席番号と ABO 血液型を保存したデータを読み込んで、おのおのの血液型の人数を出力して終了するプログラムを作成してください。
なお、ABO 血液型では、A 型、B 型、AB 型、O 型の4 分類になっています。

Input
出席番号, 血液型（1 人目のデータ：整数, 文字列）
出席番号, 血液型（2 人目のデータ：整数, 文字列）
出席番号, 血液型（3 人目のデータ：整数, 文字列）
...
...

Output
A 型の人数（整数）
B 型の人数（整数）
AB 型の人数（整数）
O 型の人数（整数）

Sample Input
1,B
2,A
3,B
4,AB
5,B
6,O
7,A
8,O
9,AB
10,A
11,A
12,B
13,AB
14,A

Output for the Sample Input
5
4
3
2
-}

import Data.List (sort, group)
import Data.List.Split (splitOn)

data Blood = A | B | AB | O deriving (Show, Read, Eq, Ord)

parse :: String -> Blood
parse =
    read . last . splitOn ","

main = do
    contents <- fmap (map parse . lines) $ readFile "0049.txt"
    mapM_ (print . length) (group . sort $ contents)

