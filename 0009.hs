{-
Prime Number

6 桁以下の正の整数 n を入力し、n 以下の素数がいくつあるかを出力するプログラムを作成して下さい。
ただし、素数とは 1 と自分自身でしか割り切れない正の整数のうち 1 をのぞいたものをいいます。
例えば 10 以下の素数は、2, 3, 5, 7 です。

Input
複数のデータセットが与えられます。各データセットに n が１行に与えられます。入力の最後まで処理して下さい。

Output
各データセットごとに、n 以下の素数の個数を１行に出力して下さい。

Sample Input
10

Output for the Sample Input
4
-}

import Data.Numbers.Primes

solve :: Int -> [Int]
solve n =
  takeWhile (< n) primes

main = do
  contents <- fmap lines $ readFile "0009.txt"
  mapM_ (print . length . solve . read) contents

