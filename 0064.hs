{-
Secret Number

新しい暗証番号は覚えにくいものです。
メモするのはダメといわれましたが、覚えられそうにありません。
そこで文章の中に数値を分けて暗証番号をメモすることにしました。
ここでは全ての数値の和が暗証番号になります。
メモされた文章を読み込んで、暗証番号を出力して終了するプログラムを作成してください。
ただし、ファイルは 1 行あたり 80 文字以内で、暗証番号は10000以下であることが保障されています。

Input
正の整数を含む文章（半角英数字ピリオドを含む文字列） ...

Output
暗証番号（文章中の正の整数の合計）

Sample Input
Thereare100yenonthetable.Iam17yearsold.
Ishouldgohomeat6pm.

Output for the Sample Input
123
-}

import Data.Char (digitToInt, isNumber)

solve :: [Int] -> Char -> [Int]
solve a c
    | isNumber c = (digitToInt c + head a * 10) : tail a
    | otherwise  = 0 : a

main = do
    contents <- readFile "0064.txt"
    print . sum . foldl solve [0] $ contents

