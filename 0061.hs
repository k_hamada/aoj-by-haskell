{-
Rank Checker

時は2020 年。
パソコン甲子園 2020 の予選結果を保存したデータがあります。
このデータには、各チームに振られる整理番号と正解数が保存されています。
参加者チーム数は不明ですが、かなり多い模様です（主催者のささやかな要望でもあります）。
正解数の多いほうから順に 1 位、2 位 ... と順位をつけていくこととします。
整理番号をキーボードから入力して、その番号のチームの順位を出力して終了するプログラムを作成してください。
ただし、パソコン甲子園 2020 の予選問題数は 30 問とします。

Input
入力データは２つの部分からなりあます。前半の部分は、予選結果のデータ、後半の部分は順位を知りたいチーム番号の問い合わせです。予選データの形式は以下の通りで、整理番号と正回数がともの 0 のときこのデータの入力が終わるものとします。

整理番号, 正解数（1 チーム目のデータ：全て整数）
整理番号, 正解数（2 チーム目のデータ：全て整数）
整理番号, 正解数（3 チーム目のデータ：全て整数）
...
...
0, 0 (入力の終わり)
続いて後半の問い合わせが複数与えられます。各問い合わせは１行に整理番号が与えられます。これを入力の最後まで処理して下さい。

Output
入力された各整理番号のチームの順位を１行に出力して下さい。詳しくはサンプルを見てください。

Sample Input
1,20
2,20
3,30
4,10
5,10
6,20
0,0
1
2
4
5

Output for the Sample Input
2
2
3
3
-}

import Data.List (sortBy, groupBy, findIndex)
import Data.List.Split (splitOn)
import Data.Function (on)

parse :: String -> ([String], [String])
parse =
    break (== "0,0") . filter (not . ("" ==)) . lines

rankGroupList :: [[Int]] -> [[Int]]
rankGroupList =
    map (map head) . reverse . groupBy ((==) `on` last) . sortBy (compare `on` last)

main = do
    (contents, _:key) <- fmap parse $ readFile "0061.txt"
    let rankList = rankGroupList . map (map read . splitOn ",") $ contents
    mapM_
      (maybe (print "0") (print . (1+)) . (\i -> findIndex (i `elem`) rankList) . read)
      key

