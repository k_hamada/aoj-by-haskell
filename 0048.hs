{-
Class

ボクシングは体重によって階級が分けられています。
体重読み込んで、それぞれについて階級が何であるかを出力して終了するプログラムを作成してください。
階級と体重の関係は以下の表のとおりとします。

階級         	 体重（kg）
light fly    	 48.00kg 以下
fly          	 48.00kg 超51.00kg 以下
bantam       	 51.00kg 超54.00kg 以下
feather      	 54.00kg 超57.00kg 以下
light        	 57.00kg 超60.00kg 以下
light welter 	 60.00kg 超64.00kg 以下
welter       	 64.00kg 超69.00 kg 以下
light middle 	 69.00kg 超75.00 kg 以下
middle       	 75.00kg 超81.00 kg 以下
light heavy  	 81.00kg 超91.00 kg 以下
heavy        	 91.00kg 超

Input
体重（1 件目のデータ：実数)
体重（2 件目のデータ：実数)
...
...

Output
階級（1 件目のデータに対する出力：文字列）
階級（2 件目のデータに対する出力：文字列）
...
...

Sample Input
60.2
70.2
48.0
80.2

Output for the Sample Input
light welter
light middle
light fly
middle
-}

solve :: Float -> String
solve x
    |           x <= 48 = "light fly"
    | 48 < x && x <= 51 = "fly"
    | 51 < x && x <= 54 = "bantam"
    | 54 < x && x <= 57 = "feather"
    | 57 < x && x <= 60 = "light"
    | 60 < x && x <= 64 = "light welter"
    | 64 < x && x <= 69 = "welter"
    | 69 < x && x <= 75 = "light middle"
    | 75 < x && x <= 81 = "middle"
    | 81 < x && x <= 91 = "light heavy"
    | 91 < x            = "heavy"


main = do
    contents <- fmap (map read . lines) $ readFile "0048.txt"
    mapM_ (putStrLn . solve) contents

