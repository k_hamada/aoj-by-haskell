{-
Circumscribed Circle of A Triangle.

平面上の点 (x1, y1)(x2, y2)(x3, y3) を頂点とした三角形の外接円の中心座標(xp, yp)と半径 r を出力して終了するプログラムを作成してください。
x1, y1, x2, y2, x3, y3, xp, yp は、それぞれ -100 以上 100 以下とします。

Input
複数のデータセットが与えられます。
最初にデータセット数 n が１行に与えられます。
各データセットは以下の形式です。

x1 y1 x2 y2 x3 y3
ここで、各値は実数とします。

Output
各データセットに対して、xp, yp, r を１つのスペースで区切って１行に出力して下さい。
小数点以下第３位まで出力して下さい。小数点第４位を四捨五入すること。

Sample Input
1
0.0 0.0 2.0 0.0 2.0 2.0

Output for the Sample Input
1.000 1.000 1.414
-}

import Data.Fixed (Milli)

solve :: Floating t => [t] -> [t]
solve [x1, y1, x2, y2, x3, y3] =
    [xp, yp, r]
  where
    a1 = 2 * (x2 - x1)
    b1 = 2 * (y2 - y1)
    c1 = x1^2 - x2^2 + y1^2 - y2^2
    a2 = 2 * (x3 - x1)
    b2 = 2 * (y3 - y1)
    c2 = x1^2 - x3^2 + y1^2 - y3^2
    xp = (b1 * c2 - b2 * c1) / (a1 * b2 - a2 * b1)
    yp = (c1 * a2 - c2 * a1) / (a1 * b2 - a2 * b1)
  -- http://www.h6.dion.ne.jp/~jpe02_tn/ki-5.html
    xy2 = ((x1-x2)^2 + (y1-y2)^2) * ((x2-x3)^2 + (y2-y3)^2) * ((x3-x1)^2 + (y3-y1)^2)
    r = (sqrt xy2 / abs(x1*(y2-y3) + x2*(y3-y1) + x3*(y1-y2))) / 2
  -- http://zigzag-engineering.blogspot.jp/2011/02/blog-post_18.html

toMilli :: Float -> Milli
toMilli =
  fromRational . toRational

main = do
  contents <- fmap (drop 1 . lines) $ readFile "0010.txt"
  mapM_ (putStrLn . unwords . map (show . toMilli) . solve . map read . words) contents

