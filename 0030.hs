{-
Sum of Integers

0 から 9 の数字から異なる n 個の数を取り出して合計が s となる組み合わせの数を出力して終了するプログラムを作成してください。
n 個の数はおのおの 0 から 9 までとし、１つの組み合わせに同じ数字は使えません。
たとえば、n が 3 で s が6 のとき、3 個の数字の合計が 6 になる組み合わせは、
  1 + 2 + 3 = 6
  0 + 1 + 5 = 6
  0 + 2 + 4 = 6
の 3 通りとなります。

Input
複数のデータセットが与えられます。各データセットに n と s が１つのスペースで区切られて１行に与えられます。
n と s が共に 0 のとき入力の最後とします（この場合は処理せずにプログラムを終了する）。

Output
各データセットに対して、n 個の整数の和が s になる組み合わせの数を１行に出力して下さい。

Sample Input
3 6
3 1
0 0

Output for the Sample Input
3
0
-}

-- http://tsumuji.cocolog-nifty.com/tsumuji/2010/02/post-09eb.html
combination :: [a] -> Int -> [[a]]
combination [] _ = [[]]
combination ns size = comb size [(ns, [])]
    where
      comb 0 xs = [a | (_, a) <- xs]
      comb c xs = comb (c - 1) $ concatMap comb' xs
      comb' (x : xs, ys) = (xs, ys ++ [x]) : comb' (xs, ys)
      comb' _ = []

solve [n, s] =
  length . filter (== s) . map sum . combination [0..9] $ n

main = do
  contents <- fmap (map (map read . words) . lines) $ readFile "0030.txt"
  mapM_ (print . solve) $ (fst . break (== [0, 0])) contents

