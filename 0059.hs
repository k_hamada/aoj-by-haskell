{-
Intersection of Rectangles

底辺が x 軸に対して平行な 2 つの長方形があります。
長方形 A の左下の座標(xa1, ya1) と右上の座標(xa2, ya2)、
長方形 B の左下の座標(xb1, yb1) と右上の座標(xb2, yb2)を読み込んで、
それぞれについて、長方形 A と長方形 B が一部でも重なっていれば YES を、
まったく重なっていなければ NO を出力して終了するプログラムを作成してください。
ただし、長方形 A と長方形 B は同じものではないとします。
また、接しているものも重なっているとみなします。

Input
xa1 ya1 xa2 ya2 xb1 yb1 xb2 yb2 ( 1 件目のデータ：全て実数)
xa1 ya1 xa2 ya2 xb1 yb1 xb2 yb2 ( 2 件目のデータ：全て実数)
xa1 ya1 xa2 ya2 xb1 yb1 xb2 yb2 ( 3 件目のデータ：全て実数)
...
...

Output
YES または NO ( 1 件目のデータに対する出力)
YES または NO ( 2 件目のデータに対する出力)
YES または NO ( 3 件目のデータに対する出力)
...
...

Sample Input
0.0 0.0 5.0 5.0 1.0 1.0 4.0 4.0
0.0 0.0 4.0 5.0 1.0 1.0 5.0 5.0
0.0 0.0 4.0 4.0 -3.0 -5.0 2.0 -1.0

Output for the Sample Input
YES
YES
NO
-}
import Data.List.Split (chunksOf)

data Point = Point Float Float deriving (Show)

parse :: String -> [Point]
parse =
    map chunkToPoint . chunksOf 2 . map read . words
  where
    chunkToPoint [x, y] = Point x y

-- http://www.c3.club.kyutech.ac.jp/gamewiki/index.php?%C5%F6%A4%BF%A4%EA%C8%BD%C4%EA#content_1_4
solve :: [Point] -> Bool
solve [a, b, c, d] =
    check a b c d
  where
    check (Point x1 y1) (Point x2 y2) (Point x3 y3) (Point x4 y4) =
      x1 < x4 && x3 < x2 && y1 < y4 && y3 < y2

yesno :: Bool -> String
yesno True = "YES"
yesno _    = "NO"

main = do
    contents <- fmap (map parse . lines) $ readFile "0059.txt"
    mapM_ (putStrLn . yesno . solve) contents
