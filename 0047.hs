{-
Cup Game

3 つのカップがふせて置かれています。
カップの置かれている場所を、順にA,B,C と呼ぶことにします。
最初はA に置かれているカップの中にボールが隠されているとします。
入れ替える２つのカップの位置を保存したデータがあります。
このデータを読み込んで、最終的にどの場所のカップにボールが隠されているかを出力して終了するプログラムを作成してください。
ただし、カップの位置を入れ替える際には、中に入っているボールも一緒に移動するものとします。

Input
入れ替える２つのカップの位置（1 回目:文字列）
入れ替える２つのカップの位置（2 回目:文字列）
入れ替える２つのカップの位置（3 回目:文字列）
...
...

Output
ボールが入っているカップの場所（文字列）

Sample Input
B,C
A,C
C,B
A,B
C,B

Output for the Sample Input
A
-}

import Data.List (elemIndex)
import Control.Monad.State (State, get, put, execState)

type Cups = [Bool]

terms :: String -> [Bool -> Bool]
terms pos =
    map (trans . (`elem` pos)) ['A'..'C']
  where
    trans bool = if bool then not else id

swap :: [Bool -> Bool] -> State Cups ()
swap fs = do
    xs <- get
    put $ zipWith ($) fs xs

view :: Cups -> Char
view cups =
    maybe '0' (['A'..'C'] !!) (elemIndex True cups)

main = do
    let cups = [True, False, False]
    contents <- fmap lines $ readFile "0047.txt"
    putChar . view $ execState (mapM (swap . terms) contents) cups

