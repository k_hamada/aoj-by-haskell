{-
Videotape

標準録画で 120 分のビデオテープがあります。
テープを完全に巻き戻した状態でビデオデッキのカウンタを 00:00:00 にし、標準録画モードで録画したところ、あるカウンタ値になりました。
このカウンタ値（時、分、秒）を入力し、残りのテープの長さ（録画可能時間）を求め、時：分：秒の形式で出力して終了するプログラムを作成して下さい。

ただし、2 時間（120分）以内の入力とします。
なお、テープ残量は標準録画モードと 3 倍録画モードの場合の２通りを計算し、出力例のように時、分、秒とも2桁ずつ出力します。
また"05"のように 10 の位が0の場合は、"0"を必ずつけてください。

入力
複数のデータセットが与えられます。各データセットは以下のとおりです。
  時（整数）分（整数）秒（整数）
入力は、３つの -1 で終わります。

出力
各データセットごとに
  テープの残りを標準録画した場合の録画可能時間　時:分:秒（半角コロン区切り）
  テープの残りを３倍録画した場合の録画可能時間　時:分:秒（半角コロン区切り）
を出力して下さい。

Sample Input
1 30 0
-1 -1 -1

Output for the Sample Input
00:30:00
01:30:00
-}

import Data.Time (DiffTime, secondsToDiffTime, timeToTimeOfDay)

parse :: String -> Integer
parse =
    toSec . map read . words
  where
    toSec [h, m, s] = h * 3600 + m * 60 + s

solve :: Integer -> Integer -> [DiffTime]
solve a b =
    [diffTape a b, diffTape (a * 3) (b * 3)]
  where
    diffTape x y = secondsToDiffTime x - secondsToDiffTime y

main = do
    contents <- fmap (fst . break (== "-1 -1 -1") . lines) $ readFile "0074.txt"
    let tapeSec = 120 * 60
    mapM_ (mapM_ (print . timeToTimeOfDay) . solve tapeSec . parse) contents

