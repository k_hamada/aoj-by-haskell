{-
Sorting Five Numbers

５つの整数 a, b, c, d, e を入力し、降順に整列した後に出力して終了するプログラムを作成して下さい。

Input
a b c d e
がそれぞれ１つの空白で区切られて１行に与えられます。

Output
降順に整列した５つの整数を１行に出力して下さい。各整数の間は１つの空白を入れて下さい。

Sample Input
3 6 9 7 5

Output for the Sample Input
9 7 6 5 3
-}

import Data.List (sort)

main = do
  contents <- fmap (map words . lines) $ readFile "0018.txt"
  mapM_ (putStrLn . unwords . reverse . sort) contents

