{-
Factorial II

  n! = n × (n - 1) × (n - 2) × ... × 3 × 2 × 1
n! を n の階乗といいます。例えば、12 の階乗は
  12! = 12 × 11 × 10 × 9 × 8 × 7 × 6 × 5 × 4 × 3 × 2 × 1 = 479001600
となり、末尾に 0 が 2 つ連続して並んでいます。
n を入力して、n! の末尾に連続して並んでいる 0 の数を出力して終了するプログラムを作成してください。
ただし、n は 2000000000 以下の正の整数とします。

Input
複数のデータが与えられます。
各データに n（整数）が１行に与えられます。
n が 0 の時入力の最後とします。

Output
各データに対して n! の末尾に連続して並んでいる 0 の数を１行に出力して下さい。

Sample Input
2
12
10000
0

Output for the Sample Input
0
2
2499
-}

factorial 0 = 1
factorial n = n * factorial (n-1)

solve :: Integer -> Int
solve =
    tailZeroLength . factorial
  where
    tailZeroLength = length . takeWhile (== '0') . reverse . show

main = do
    contents <- fmap (fst . break (== 0) . map read . lines) $ readFile "0052.txt"
    mapM_ (print . solve) contents

