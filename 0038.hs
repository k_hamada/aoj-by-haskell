{-
Poker Hand

ポーカーの手札データを読み込んで、それぞれについてその役を出力して終了するプログラムを作成してください。
ただし、この問題では、以下のルールに従います。

ポーカーはトランプ 5 枚で行う競技です。
同じ数字のカードは 5 枚以上ありません。
ジョーカーは無いものとします。

以下のポーカーの役だけを考えるものとします。(下にいくにつれて役が高くなります。)
  役なし(以下に挙げるどれにも当てはまらない)
  ワンペア（2 枚の同じ数字のカードが1 組ある）
  ツーペア（2 枚の同じ数字のカードが2 組ある）
  スリーカード（3 枚の同じ数字のカードが1 組ある）
  ストレート（5 枚のカードの数字が連続している）
    A を含むストレートの場合、A で終わる並びもストレートとします。
    つまり、A を含むストレート は、A 2 3 4 5 と10 J Q K A の２種類です。
    J Q K A 2 などのように、A をまたぐ並び はストレートではありません。（この場合、役なしになります）。
  フルハウス（3 枚の同じ数字のカードが1 組と、残りの2 枚が同じ数字のカード）
  フォーカード（4 枚の同じ数字のカードが1 組ある）

Input
トランプのJ(ジャック) を11、Q(クイーン) を12、K(キング) を13、A（エース）を 1 のそれぞれの数字で表すこととします。
入力例を見てください。

手札1, 手札2, 手札3, 手札4, 手札5（1 件目のデータ：正の整数）
手札1, 手札2, 手札3, 手札4, 手札5（2 件目のデータ：正の整数）
手札1, 手札2, 手札3, 手札4, 手札5（3 件目のデータ：正の整数）
手札1, 手札2, 手札3, 手札4, 手札5（4 件目のデータ：正の整数）
...
...

Output
手札によってできるもっとも高い役をひとつ出力するようにしてください。
たとえば、3 3 2 3 3 という手札であったときは、two pair ではなく four cards です。
出力例を見てください。

Sample Input
1,2,3,4,1
2,3,2,3,12
12,13,11,12,12
7,6,7,6,7
3,3,2,3,3
6,7,8,9,10
11,12,10,1,13
11,12,13,1,2

Output for the Sample Input
one pair
two pair
three card
full house
four card
straight
straight
null
-}

import Data.List (sort, group)
import Data.List.Split (splitOn)

duplication :: [Int] -> [Int]
duplication = sort . filter (> 1) . map length . group . sort

isStraight :: [Int] -> Bool
isStraight xs
    | straightA = True
    | otherwise = length makeLevel == 1
  where
    xs'       = sort xs
    l         = length xs
    straightA = [1,10,11,12,13] == xs'
    makeLevel = group . zipWith (+) [l-1, l-2..0] . sort $ xs'

solve :: [Int] -> String
solve xs
    | ys == [4]     = "four card"
    | ys == [2, 3]  = "full house"
    | ys == [3]     = "three card"
    | ys == [2, 2]  = "two pair"
    | ys == [2]     = "one pair"
    | isStraight xs = "straight"
    | otherwise     = "null"
  where
    ys = duplication xs

main = do
    contents <- fmap lines $ readFile "0038.txt"
    mapM_ (putStrLn . solve . map read . splitOn ",") contents
