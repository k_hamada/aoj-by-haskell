{-
Sequence

次のように定義されている数列があります。

すべての偶数番目の項は一つ前の項に 2 を掛けたものと等しい数である。
すべての奇数番目の項は一つ前の項を 3 で割ったものと等しい数である。

この数列の初項 a を読み込み、それぞれについて、初項から第 10 項までの和 s(10) を出力して終了するプログラムを作成してください。
ただし、a は1.0 以上10.0 以下とします。


Input
定義された数列の初項 a（1 件目のデータ：実数）
定義された数列の初項 a（2 件目のデータ：実数）
定義された数列の初項 a（3 件目のデータ：実数）
...
...

Output
s(10)（1 件目のデータに対する出力）
s(10)（2 件目のデータに対する出力）
s(10)（3 件目のデータに対する出力）
...
...

出力は0.000001以下の誤差を含んでもよい。

Sample Input
1.0
2.0
3.0

Output for the Sample Input
7.81481481
15.62962963
23.44444444
-}

solve :: Int -> Double -> [Double]
solve i n
    | odd i     = n : solve (i + 1) (n * 2)
    | otherwise = n : solve (i + 1) (n / 3)

main = do
    contents <- fmap (map read . lines) $ readFile "0055.txt"
    mapM_ (print . sum . take 10 . solve 1) contents

