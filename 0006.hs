{-
Reverse Sequence

文字列 str を入力したとき、その文字列を逆順に出力するプログラムを作成して下さい。文字は半角英数字のみで、20 文字以内とします。

Input
文字列 str

Output
str の逆順

Sample Input
w32nimda

Output for the Sample Input
admin23w
-}

main = do
  contents <- fmap lines $ readFile "0006.txt"
  mapM_ (putStrLn . reverse) contents

