{-
Prime Number II

素数というのは、１よりも大きくそれ自身か 1 でしか割りきれない整数をいいます。
例えば、2 は、2 と 1でしか割り切れないので素数ですが、12 は、12 と 1 のほかに、2, 3, 4, 6 で割りきれる数なので素数ではありません。
整数 n を入力したとき、n より小さい素数のうち最も大きいものと、
 n より大きい素数のうち最も小さいものを出力して終了するプログラムを作成してください。
ただし、n は 3 以上 50000 以下とします。

Input
複数のデータセットが与えられます。
各データセットに n が１行に与えられます。
入力の最後まで処理して下さい。

Output
各データセットに対して、n より小さい素数のうち最大のものと、
 n より大きい素数のうち最小のものを１つのスペースで区切って１行に出力して下さい。

Sample Input
19
3517

Output for the Sample Input
17 23
3511 3527
-}

import Data.List (span)
import Data.Functor ((<$>))
import Data.Numbers.Primes

solve :: ([Int], [Int]) -> String
solve (x, y) =
    unwords . map show $ [x', y']
  where
    x' = last x
    y' = y !! 1

main = do
    contents <- fmap (map read . lines) $ readFile "0044.txt"
    mapM_ (putStrLn . solve . flip span primes) $ (>) <$> contents

