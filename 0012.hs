{-
A Point in a Triangle

平面上に (x1, y1), (x2, y2), (x3, y3) を頂点とした三角形と点 P(xp, yp) があります。
点 P が三角形の内部(三角形の頂点や辺上は含まない)にあるなら YES、三角形の外部にあるなら NO を出力して終了するプログラムを作成して下さい。

なお、与える点 P は三角形の頂点や辺の上にはないものとし、x1, y1, x2, y2, x3, y3 xp, yp は、それぞれ -100 以上 100 以下とします。

Input
複数のデータセットが与えられます。各データセットの形式は以下のようになっています。
  x1 y1 x2 y2 x3 y3 xp yp
与えられる入力は全て実数です。入力の最後まで処理して下さい。

Output
書くデータセットに対して、YES または NO を１行に出力して下さい。

Sample Input
0.0 0.0 2.0 0.0 2.0 2.0 1.5 0.5
0.0 0.0 1.0 4.0 5.0 3.0 -1.0 3.0

Output for the Sample Input
YES
NO
-}

-- http://www.interq.or.jp/moonstone/person/del/atari2.htm
isInnerTriangle :: [Float] -> Bool
isInnerTriangle [x1, y1, x2, y2, x3, y3, xp, yp]
  | (x3*(y1-y2)+y3*(x2-x1)+x1*y2-x2*y1) * (xp*(y1-y2)+yp*(x2-x1)+x1*y2-x2*y1) < 0 = False
  | (x1*(y2-y3)+y1*(x3-x2)+x2*y3-x3*y2) * (xp*(y2-y3)+yp*(x3-x2)+x2*y3-x3*y2) < 0 = False
  | (x2*(y3-y1)+y2*(x1-x3)+x3*y1-x1*y3) * (xp*(y3-y1)+yp*(x1-x3)+x3*y1-x1*y3) < 0 = False
  | otherwise                                                                     = True

yesno :: Bool -> String
yesno True = "YES"
yesno _    = "NO"

main = do
  contents <- fmap lines $ readFile "0012.txt"
  mapM_ (putStrLn . yesno . isInnerTriangle . map read . words) contents

