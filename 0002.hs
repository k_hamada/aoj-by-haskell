{-
Digit Number

与えられた２つの整数 a と b の和の桁数を出力して終了するプログラムを作成して下さい。

Input
複数のデータセットが与えられます。各データセットは１行に与えられます。
各データセットは２つの整数 a と b が１つのスペースで区切られて与えられます。
入力の終わりまで処理して下さい。a と b は非負の整数とします。

Output
各データセットごとに、a + b の桁数を出力して下さい。
-}

main = do
  contents <- fmap lines $ readFile "0002.txt"
  mapM_ (print . length . show . sum . map read . words) contents

