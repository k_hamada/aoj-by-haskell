{-
Goldbach's Conjecture

4 以上の偶数は 2 つの素数の和で表すことができるということが知られています。
これはゴールドバッハ予想といい、コンピュータの計算によりかなり大きな数まで正しいことが確かめられています。
例えば、10 は、7 + 3、5 + 5 の 2 通りの素数の和で表すことができます。
整数 n を入力したとき、n を 2 つの素数の和で表す組み合わせ数が何通りあるかを出力して終了するプログラムを作成してください。
ただし、n は 4 以上、50000 以下とします。
また、入力される n は偶数であるとはかぎりません。

Input
複数のデータセットが与えられます。各データセットに n が１行に与えられます。n が 0 のとき入力の最後とします。

Output
各データセットに対して、n を 2 つの素数の和で表す組み合わせ数を１行に出力して下さい。

Sample Input
10
11
0

Output for the Sample Input
2
0
-}

import Data.Numbers.Primes

goldbach n =
    length . filter (== n) . map sum . (`rcomb` 2) . takeWhile (< n) $ primes
  where
    rcomb [] _ = []
    rcomb xs 0 = [[]]
    rcomb xs 1 = [[x] | x <- xs]
    rcomb xxs@(x:xs) n = map (x:) (rcomb xxs (n-1)) ++ rcomb xs n
    -- http://www.sampou.org/cgi-bin/haskell.cgi?Programming%3a%E7%8E%89%E6%89%8B%E7%AE%B1%3a%E7%B5%84%E5%90%88%E3%81%9B#H-h4cyum91co7e

solve :: Int -> Int
solve n
    | even n    = goldbach n
    | otherwise = 0

main = do
  contents <- fmap (fst . break (== 0) . map read . lines) $ readFile "0056.txt"
  mapM_ (print . solve) contents

